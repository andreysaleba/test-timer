import React from 'react';
import './App.css';

import Counter from './components/counter'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src='https://media.giphy.com/media/xUOxeXt41UOYRusw4E/giphy.gif' className="App-logo" alt="logo" />
        <p>
          Test timer application
        </p>
        <Counter/>
      </header>
    </div>
  );
}

export default App;
