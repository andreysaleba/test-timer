import {DECREMENT, RESET, SET} from "./actionTypes";
import {store} from '../index';


export const decrement = () => {
  const {counter: {time}} = store.getState();
  if (time <= 1) {
    store.dispatch(stopTimer());
  }
  return {
    type: DECREMENT
  };
};

export const stopTimer = () => {
  const {counter: {timerId}} = store.getState();
  clearInterval(timerId);
  return {type: RESET};
};


export const startTimer = (time) => {
  const {counter: {timerId}} = store.getState();
  if (timerId) {
    store.dispatch(stopTimer());
  }
  return {type: SET, time, timerId: setInterval(() => store.dispatch(decrement()), 1000)};
};
