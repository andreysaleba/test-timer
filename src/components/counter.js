import React, {useState} from 'react';
import {connect} from 'react-redux';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

import './style.css';
import {decrement, stopTimer, startTimer} from './../actions';

const TIMER_STEP = 5;

const OPTIONS = Array.from({length: 12}).map((elem, index) => (index * TIMER_STEP).toString());

const Counter = ({time, startTimer, stopTimer}) => {
  const [defaultTime, defaultTimeChange] = useState(OPTIONS[0]);

  const handleOnChange = (event) => {
    defaultTimeChange(event.value);
  };

  const handleStartTimer = (event) => {
    event.preventDefault();
    startTimer(parseInt(defaultTime));
  };

  const handleStopTimer = (event) => {
    event.preventDefault();
    stopTimer();
  };

  const handleContinueTimer = (event) => {
    event.preventDefault();
    startTimer(time);
  };

  return (
    <>
      <form onSubmit={handleStartTimer}>
        <Dropdown options={OPTIONS} onChange={handleOnChange} value={defaultTime} placeholder="Select an option"/>
        <p>Seconds left: {time}</p>
        <input className="green button" type="submit" value="Start/Restart" onClick={handleStartTimer}/>
        <input className="red button" type="submit" value="Stop" onClick={handleStopTimer}/>
        <input className="blue button" type="submit" value="Continue" onClick={handleContinueTimer}/>
      </form>
    </>
  );
};


const mapStateToProps = ({counter: {time}}) => ({time});

const mapDispatchToProps = {decrement, stopTimer, startTimer};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter);
