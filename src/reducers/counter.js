import {DECREMENT, RESET, SET} from "../actions/actionTypes";

const initialState = {
  time: 0,
  timerId: null

};

export default function (state = initialState, action) {
  switch (action.type) {
    case DECREMENT: {
      return {
        ...state, time: state.time - 1
      };
    }
    case RESET: {
      return {
        ...state, timerId: null
      };
    }
    case SET: {
      return {
        ...state, time: action.time, timerId: action.timerId
      };
    }
    default:
      return state;
  }
}
